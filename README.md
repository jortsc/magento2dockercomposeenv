# Magento 2 Docker Compose Recipe
Basic recipe to install Magento 2 latest version (2.4) instances and develop modules locally. It can be extend by adding whatever service the project needs. Just adding its section on the recipe.

* Apache php 7.4
* MariaDB 10.04
* ElasticSearchphpMyAdmin
* phpMyAdmin (although you can point your favorite editor to the container, DBeaver, SqlPro..)

## Set up

### Spin up
```docker-compose up -d --build```

### Connect with web container in order to install Magento 2
```docker exec -it web bash```

### Create project
```composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition ~/path/to/magento2 ```

### Install command with sample data
```
php bin/magento setup:install \
--admin-firstname="Jose Manuel" \
--admin-lastname=Orts \
--admin-email=jortsc@gmail.com \
--admin-user=jortsc \
--admin-password=admin1234 \
--base-url=https://mage.dev.local \
--base-url-secure=https://mage.dev.local \
--backend-frontname=admin \
--db-host=mariaDB \
--db-name=magento_dev_local \
--db-user=root \
--db-password=root \
--use-rewrites=1 \
--language=es_ES \
--currency=EUR \
--timezone=Europe/Madrid \
--use-secure-admin=0 \
--admin-use-security-key=0 \
--session-save=files \
--elasticsearch-host=elasticsearch \
--use-sample-data
```
You can remove `--use-sample-data` flag in order to install a clean one.

Once finished you can browse to the url you set in the `/etc/hosts` in this case `https://mage.dev.local`.
First time you go to access the site, it might take a few of minutes for the page to load. This is because nothing is cached yet and the Magento system is automatically generating files as the page loads. Subsequent page loads will be faster. 

Additionally, because the web container uses a self-signed SSL certificate, the browser will likely present you with a security alert the first time you visit the URL. Just follow any prompts to add an exception so that you can proceed to the local website.

If you'd like to use a database manager like DBeaver or SqlPro database is accesible from `127.0.0.1:3306`. There's also an instance of phpMyAdmin accesible from `127.0.0.1:8080` that you can use or remove from the docker composer if you're going to use a DB manager.


## Some commands

### Spin Up
``docker-compose up -d --build``

### Tear Down
```docker-compose down```

### Start
```docker-compose start```

### Stop
```docker-compose stop```

### Connect to web container
```docker exec -it web bash``` 

### Connect to database container
```docker exec -it mariaDB bash```
